//Leer el archivo XML a pintar
$.ajax({
  url: 'Prueba2.xml', // name of file you want to parse
  dataType: "xml", // type of file you are trying to read
  success: parse, // name of the function to call upon success
  error: function(){alert("Error: Something went wrong");}
});

function parse(doc){
  
  var paper = new Raphael(document.getElementById('canvas'), 2000, 2000);
  


  /////////////
  //PROPIEDADES
  /////////////

  
  //Eventos inicio
  var startEvents = doc.getElementsByTagName("startEvent");
  var eventosInicio = new Array();
  
  for(var i=0; i<startEvents.length; i++)
  {
      var startEvent = startEvents.item(i);

      var id = startEvent.attributes.getNamedItem('id').nodeValue;
      eventosInicio.push(id);
  }
  
  //Tareas
  var tasks = doc.getElementsByTagName("task");
  var tareas = new Array();
  
  for(var i=0; i<tasks.length; i++)
  {
      var task = tasks.item(i);
      var id = task.attributes.getNamedItem('id').nodeValue;
      tareas.push(id);
  }
  
  //Eventos fin
  var endEvents = doc.getElementsByTagName("endEvent");
  var eventosFin = new Array();
  
  for(var i=0; i<endEvents.length; i++)
  {
      var endEvent = endEvents.item(i);

      var id = endEvent.attributes.getNamedItem('id').nodeValue;

      eventosFin.push(id);
  }

  //Eventos intermedios
  var intermediateEvents = doc.getElementsByTagName("intermediateThrowEvent");
  var eventosIntermedios = new Array();
  
  for(var i=0; i<intermediateEvents.length; i++)
  {
      var intermediateEvent = intermediateEvents.item(i);

      var id = intermediateEvent.attributes.getNamedItem('id').nodeValue;

      eventosIntermedios.push(id);
  }

  
  //GATEWAYS 3 tipos

  var exGateways = doc.getElementsByTagName("exclusiveGateway");
  var exclusivas = new Array();
  
  for(var i=0; i<exGateways.length; i++)
  {
      var exGateway = exGateways.item(i);

      var id = exGateway.attributes.getNamedItem('id').nodeValue;

      exclusivas.push(id);
  }

  var inGateways = doc.getElementsByTagName("inclusiveGateway");
  var inclusivas = new Array();
  
  for(var i=0; i<inGateways.length; i++)
  {
      var inGateway = inGateways.item(i);

      var id = inGateway.attributes.getNamedItem('id').nodeValue;

      inclusivas.push(id);
  }
  
  var parGateways = doc.getElementsByTagName("parallelGateway");
  var paralelas = new Array();
  
  for(var i=0; i<parGateways.length; i++)
  {
      var parGateway = parGateways.item(i);

      var id = parGateway.attributes.getNamedItem('id').nodeValue;

      paralelas.push(id);
  } 




  
  //////////////////
  //////////////////
  //DIBUJAR DIAGRAMA
  //////////////////
  //////////////////
  
  var flows = doc.getElementsByTagName("sequenceFlow");
  var flujosTarget = new Array();
  var flujosSource = new Array();
  
  //Flechas
  for(var i=0; i<flows.length; i++)
  {
      var flow = flows.item(i);
      var targetId = flow.attributes.getNamedItem('targetRef').nodeValue;
      var sourceId = flow.attributes.getNamedItem('sourceRef').nodeValue;
      flujosTarget.push(targetId);
      flujosSource.push(sourceId);    
  }
  
  
  // Aquí tenemos los elementos con sus valores
  var shapes = doc.getElementsByTagName("BPMNShape");

  var centros = new Array();
  for(var i=0; i<shapes.length; i++){
    var shape = shapes.item(i);
    var bpmnElement = shape.attributes.getNamedItem('bpmnElement').nodeValue;

    var bounds = shape.children[0];

    
    var atts = bounds.attributes;
    
    var x = atts.getNamedItem('x').nodeValue;
    var y = atts.getNamedItem('y').nodeValue;
    x = parseInt(x.substring(0,x.indexOf(".")+3));
    y = parseInt(y.substring(0,y.indexOf(".")+3));

    
    var height = parseInt(atts.getNamedItem('height').nodeValue);
    var width = parseInt(atts.getNamedItem('width').nodeValue);

    if(eventosInicio.indexOf(bpmnElement)!=-1)
    {
      paper.rect(x,y,width,height, x)
      .attr({
            fill: '#ffffff',
            'stroke-width': 1
        })     
      
    }
    else if(eventosFin.indexOf(bpmnElement)!=-1)
    {      
      paper.rect(x,y,width,height, x)
      .attr({
            fill: '#ffffff',
            'stroke-width': 4
      })
    }
    else if(eventosIntermedios.indexOf(bpmnElement)!=-1)
    {      
      paper.rect(x,y,width,height, x)
      .attr({
            fill: '#ffffff'//#D20612
        })

      paper.circle(x+(width/2), y+(height/2), width/2.5).attr({ 'stroke-width':2 })
      .attr({
            fill: '#ffffff'
      })
    }
    else if(tareas.indexOf(bpmnElement)!=-1)
    {
      //Dibujar elemento
      el = paper.rect(x, y, width, height, x/150)

      //Poner Icono
      pintarIconoTarea(el, "usuario")

      //Cuadro subproceso
      anchosubproceso = width/5
      posicionxsubproceso = x+(width/2)-(anchosubproceso/2)
      posicionysubproceso = y+(height/1.5)
      el = paper.rect(posicionxsubproceso, posicionysubproceso, anchosubproceso, anchosubproceso)

      //Icono cuadro subproceso
      el.attr('font-size', 40);
      el.attr('fill', '#fff');
      el.attr('font-family','FontAwesome');
      fbicon = "m 23, 10 0,4.5 -4.5,0 0,1 4.5,0 0,4.5 1,0 0,-4.5 4.5,0 0,-1 -4.5,0 0,-4.5 -1,0 z";         
      el2 = paper.path(fbicon).attr({fill: "black", stroke: "black"}).translate(posicionxsubproceso-14,posicionysubproceso-5)
      el2.node.setAttribute("class", "icongateway")

      //Para poner texto:      
      texto = {}
      mensaje = "El texto bien de la tarea 1 es muy guay"
      largomaximo = width/5
      fin = largomaximo

      for(j=0;j<mensaje.length;j++) {

        texto[j] = mensaje.substring(0, fin)
        mensaje = mensaje.substring(largomaximo)

        paper.text(x+(width/2), y+(height/2.5)+(j*10), texto[j])
        .attr({
          fill: 'black'
         })

        fin = fin + largomaximo
      }
      
    }
    else if(exclusivas.indexOf(bpmnElement)!=-1 || paralelas.indexOf(bpmnElement)!=-1 || inclusivas.indexOf(bpmnElement)!=-1)
    {
      var exPintado = paper.rect(x, y, width, height)

      //Poner Icono
      if(exclusivas.indexOf(bpmnElement)!=-1)
        pintarIconoGateway(el, "exclusive")
      else if (inclusivas.indexOf(bpmnElement)!=-1)
        paper.circle(x+(width/2), y+(height/2), width/2.5).attr({ 'stroke-width':2 })
      else
        pintarIconoGateway(el, "parallel")

      exPintado.node.setAttribute("class","rombo");
    }
    
    var elemento = {}
    elemento.id = bpmnElement;
    elemento.x = x+width/2;
    elemento.y = y+height/2;
    centros.push(elemento);
  }
  
  // Aquí tenemos los elementos con sus valores
  var edges = doc.getElementsByTagName("BPMNEdge");
  
  for(var i=0; i<edges.length; i++){
    var edge = edges.item(i);
    var bpmnElement = edge.attributes.getNamedItem('bpmnElement').nodeValue;

    var waypoint1 = edge.children[0];
    var waypoint2 = edge.children[edge.children.length-1];
    
    var atts1 = waypoint1.attributes;
    var atts2 = waypoint2.attributes;
    
    var x1 = atts1.getNamedItem('x').nodeValue;
    var y1 = atts1.getNamedItem('y').nodeValue;

    x1 = parseInt(x1.substring(0,x1.indexOf(".")+3));
    y1 = parseInt(y1.substring(0,y1.indexOf(".")+3));

    
    var x2 = atts2.getNamedItem('x').nodeValue;
    var y2 = atts2.getNamedItem('y').nodeValue;
    x2 = parseInt(x2.substring(0,x2.indexOf(".")+3));
    y2 = parseInt(y2.substring(0,y2.indexOf(".")+3));

    var cantidadSobrante = 0;
    
    if(exclusivas.indexOf(flujosSource[i])!=-1 || paralelas.indexOf(flujosSource[i])!=-1 || inclusivas.indexOf(flujosSource[i])!=-1)
    {
      cantidadSobrante = (Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2))-width)/2;
    }
    if(exclusivas.indexOf(flujosTarget[i])!=-1 || paralelas.indexOf(flujosTarget[i])!=-1 || inclusivas.indexOf(flujosTarget[i])!=-1)
    {
      var arrayCentros = centros.filter(function( obj ) {
        return obj.id == flujosTarget[i];
      });
      var centro = arrayCentros[0];
      
      var diagonal = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));

      if(y2>centro.y && x2==centro.x)
      {
        
        var prueba = paper.path("M "+(x1).toString()+" "+(y1+cantidadSobrante).toString()+" l "+(x2-x1).toString()+" "+(y2-y1-cantidadSobrante+(diagonal-width)/2).toString()).attr({ 'stroke-width':2, 'arrow-end': 'classic-wide-long'});

      }
      else if(y2<centro.y && x2==centro.x)
      {
        //var prueba = paper.path("M "+(x1).toString()+" "+(y1-cantidadSobrante).toString()+" l "+(x2-x1).toString()+" "+(y2-y1+cantidadSobrante-(diagonal-width)/2).toString()).attr({ 'stroke-width':2, 'arrow-end': 'classic-wide-long'});
        var prueba = paper.path("M "+(x1).toString()+" "+(y1-cantidadSobrante).toString()+" l "+(x2-x1).toString()+" "+(y2-y1+cantidadSobrante-(diagonal-width)/2).toString()).attr({ 'stroke': 'black', 'stroke-width':2, 'arrow-end': 'block-wide-long', 'arrow-start': 'default2'});

      }
      else if(y2==centro.y && x2>centro.x)
      {
        var prueba = paper.path("M "+(x1+cantidadSobrante).toString()+" "+(y1).toString()+" l "+(x2-x1-cantidadSobrante-(diagonal-width)/2).toString()+" "+(y2-y1).toString()).attr({ 'stroke-width':2, 'arrow-end': 'classic-wide-long'});
      }
      else if(y2==centro.y && x2<centro.x)
      {
        
        var prueba = paper.path("M "+(x1-cantidadSobrante).toString()+" "+(y1).toString()+" l "+(x2-x1+cantidadSobrante-(diagonal-width)/2).toString()+" "+(y2-y1).toString()).attr({ 'stroke-width':2, 'arrow-end': 'classic-wide-long'});
      }
      
    }
    else
    {
      
      var arrayCentros = centros.filter(function( obj ) {
        return obj.id == flujosSource[i];
      });
      var centro = arrayCentros[0];

     //var indice = centros.indexOf(flujosSource[i]);

      if(y1>centro.y && x1==centro.x)
      {

        var prueba = paper.path("M "+(x1).toString()+" "+(y1+cantidadSobrante).toString()+" l "+(x2-x1).toString()+" "+(y2-y1).toString()).attr({ 'stroke-width':2, 'arrow-end': 'classic-wide-long'});
      }
      else if(y1<centro.y && x1==centro.x)
      {       
 
        var prueba = paper.path("M "+(x1).toString()+" "+(y1-cantidadSobrante).toString()+" l "+(x2-x1).toString()+" "+(y2-y1).toString()).attr({ 'stroke-width':2, 'arrow-end': 'classic-wide-long'});
      }
      else if(y1==centro.y && x1>centro.x)
      {        
   
        var prueba = paper.path("M "+(x1+cantidadSobrante).toString()+" "+(y1).toString()+" l "+(x2-x1-cantidadSobrante).toString()+" "+(y2-y1).toString()).attr({ 'stroke-width':2, 'arrow-end': 'classic-wide-long'});

      }
      else if(y1==centro.y && x1<centro.x)
      {        

        var prueba = paper.path("M "+(x1-cantidadSobrante).toString()+" "+(y1).toString()+" l "+(x2-x1+cantidadSobrante).toString()+" "+(y2-y1).toString()).attr({ 'stroke-width':2, 'arrow-end': 'classic-wide-long'});

      }
      
    }
  }

  ////////
  //ICONOS
  ////////


  function pintarIconoTarea(el, tipo) {
    el.attr('font-size', 40);
    el.attr('fill', '#fff');
    el.attr('font-family','FontAwesome');
    
    if(tipo == "usuario")
     fbicon = "M12.075,10.812c1.358-0.853,2.242-2.507,2.242-4.037c0-2.181-1.795-4.618-4.198-4.618S5.921,4.594,5.921,6.775c0,1.53,0.884,3.185,2.242,4.037c-3.222,0.865-5.6,3.807-5.6,7.298c0,0.23,0.189,0.42,0.42,0.42h14.273c0.23,0,0.42-0.189,0.42-0.42C17.676,14.619,15.297,11.677,12.075,10.812 M6.761,6.775c0-2.162,1.773-3.778,3.358-3.778s3.359,1.616,3.359,3.778c0,2.162-1.774,3.778-3.359,3.778S6.761,8.937,6.761,6.775 M3.415,17.69c0.218-3.51,3.142-6.297,6.704-6.297c3.562,0,6.486,2.787,6.705,6.297H3.415z";
    else if (tipo == "servicio")
      fbicon = "M17.498,11.697c-0.453-0.453-0.704-1.055-0.704-1.697c0-0.642,0.251-1.244,0.704-1.697c0.069-0.071,0.15-0.141,0.257-0.22c0.127-0.097,0.181-0.262,0.137-0.417c-0.164-0.558-0.388-1.093-0.662-1.597c-0.075-0.141-0.231-0.22-0.391-0.199c-0.13,0.02-0.238,0.027-0.336,0.027c-1.325,0-2.401-1.076-2.401-2.4c0-0.099,0.008-0.207,0.027-0.336c0.021-0.158-0.059-0.316-0.199-0.391c-0.503-0.274-1.039-0.498-1.597-0.662c-0.154-0.044-0.32,0.01-0.416,0.137c-0.079,0.106-0.148,0.188-0.22,0.257C11.244,2.956,10.643,3.207,10,3.207c-0.642,0-1.244-0.25-1.697-0.704c-0.071-0.069-0.141-0.15-0.22-0.257C7.987,2.119,7.821,2.065,7.667,2.109C7.109,2.275,6.571,2.497,6.07,2.771C5.929,2.846,5.85,3.004,5.871,3.162c0.02,0.129,0.027,0.237,0.027,0.336c0,1.325-1.076,2.4-2.401,2.4c-0.098,0-0.206-0.007-0.335-0.027C3.001,5.851,2.845,5.929,2.77,6.07C2.496,6.572,2.274,7.109,2.108,7.667c-0.044,0.154,0.01,0.32,0.137,0.417c0.106,0.079,0.187,0.148,0.256,0.22c0.938,0.936,0.938,2.458,0,3.394c-0.069,0.072-0.15,0.141-0.256,0.221c-0.127,0.096-0.181,0.262-0.137,0.416c0.166,0.557,0.388,1.096,0.662,1.596c0.075,0.143,0.231,0.221,0.392,0.199c0.129-0.02,0.237-0.027,0.335-0.027c1.325,0,2.401,1.076,2.401,2.402c0,0.098-0.007,0.205-0.027,0.334C5.85,16.996,5.929,17.154,6.07,17.23c0.501,0.273,1.04,0.496,1.597,0.66c0.154,0.047,0.32-0.008,0.417-0.137c0.079-0.105,0.148-0.186,0.22-0.256c0.454-0.453,1.055-0.703,1.697-0.703c0.643,0,1.244,0.25,1.697,0.703c0.071,0.07,0.141,0.15,0.22,0.256c0.073,0.098,0.188,0.152,0.307,0.152c0.036,0,0.073-0.004,0.109-0.016c0.558-0.164,1.096-0.387,1.597-0.66c0.141-0.076,0.22-0.234,0.199-0.393c-0.02-0.129-0.027-0.236-0.027-0.334c0-1.326,1.076-2.402,2.401-2.402c0.098,0,0.206,0.008,0.336,0.027c0.159,0.021,0.315-0.057,0.391-0.199c0.274-0.5,0.496-1.039,0.662-1.596c0.044-0.154-0.01-0.32-0.137-0.416C17.648,11.838,17.567,11.77,17.498,11.697 M16.671,13.334c-0.059-0.002-0.114-0.002-0.168-0.002c-1.749,0-3.173,1.422-3.173,3.172c0,0.053,0.002,0.109,0.004,0.166c-0.312,0.158-0.64,0.295-0.976,0.406c-0.039-0.045-0.077-0.086-0.115-0.123c-0.601-0.6-1.396-0.93-2.243-0.93s-1.643,0.33-2.243,0.93c-0.039,0.037-0.077,0.078-0.116,0.123c-0.336-0.111-0.664-0.248-0.976-0.406c0.002-0.057,0.004-0.113,0.004-0.166c0-1.75-1.423-3.172-3.172-3.172c-0.054,0-0.11,0-0.168,0.002c-0.158-0.312-0.293-0.639-0.405-0.975c0.044-0.039,0.085-0.078,0.124-0.115c1.236-1.236,1.236-3.25,0-4.486C3.009,7.719,2.969,7.68,2.924,7.642c0.112-0.336,0.247-0.664,0.405-0.976C3.387,6.668,3.443,6.67,3.497,6.67c1.75,0,3.172-1.423,3.172-3.172c0-0.054-0.002-0.11-0.004-0.168c0.312-0.158,0.64-0.293,0.976-0.405C7.68,2.969,7.719,3.01,7.757,3.048c0.6,0.6,1.396,0.93,2.243,0.93s1.643-0.33,2.243-0.93c0.038-0.039,0.076-0.079,0.115-0.123c0.336,0.112,0.663,0.247,0.976,0.405c-0.002,0.058-0.004,0.114-0.004,0.168c0,1.749,1.424,3.172,3.173,3.172c0.054,0,0.109-0.002,0.168-0.004c0.158,0.312,0.293,0.64,0.405,0.976c-0.045,0.038-0.086,0.077-0.124,0.116c-0.6,0.6-0.93,1.396-0.93,2.242c0,0.847,0.33,1.645,0.93,2.244c0.038,0.037,0.079,0.076,0.124,0.115C16.964,12.695,16.829,13.021,16.671,13.334 M10,5.417c-2.528,0-4.584,2.056-4.584,4.583c0,2.529,2.056,4.584,4.584,4.584s4.584-2.055,4.584-4.584C14.584,7.472,12.528,5.417,10,5.417 M10,13.812c-2.102,0-3.812-1.709-3.812-3.812c0-2.102,1.71-3.812,3.812-3.812c2.102,0,3.812,1.71,3.812,3.812C13.812,12.104,12.102,13.812,10,13.812";
    else if (tipo == "mensaje")
      fbicon = "M17.388,4.751H2.613c-0.213,0-0.389,0.175-0.389,0.389v9.72c0,0.216,0.175,0.389,0.389,0.389h14.775c0.214,0,0.389-0.173,0.389-0.389v-9.72C17.776,4.926,17.602,4.751,17.388,4.751 M16.448,5.53L10,11.984L3.552,5.53H16.448zM3.002,6.081l3.921,3.925l-3.921,3.925V6.081z M3.56,14.471l3.914-3.916l2.253,2.253c0.153,0.153,0.395,0.153,0.548,0l2.253-2.253l3.913,3.916H3.56z M16.999,13.931l-3.921-3.925l3.921-3.925V13.931z";
    else
      fbicon = "M18.344,16.174l-7.98-12.856c-0.172-0.288-0.586-0.288-0.758,0L1.627,16.217c0.339-0.543-0.603,0.668,0.384,0.682h15.991C18.893,16.891,18.167,15.961,18.344,16.174 M2.789,16.008l7.196-11.6l7.224,11.6H2.789z M10.455,7.552v3.561c0,0.244-0.199,0.445-0.443,0.445s-0.443-0.201-0.443-0.445V7.552c0-0.245,0.199-0.445,0.443-0.445S10.455,7.307,10.455,7.552M10.012,12.439c-0.733,0-1.33,0.6-1.33,1.336s0.597,1.336,1.33,1.336c0.734,0,1.33-0.6,1.33-1.336S10.746,12.439,10.012,12.439M10.012,14.221c-0.244,0-0.443-0.199-0.443-0.445c0-0.244,0.199-0.445,0.443-0.445s0.443,0.201,0.443,0.445C10.455,14.021,10.256,14.221,10.012,14.221";
      
    paper.path(fbicon).attr({fill: "black", stroke: "black"}).translate(x+5,y+3)
  }

  function pintarIconoGateway(el, tipo) {
    el.attr('font-size', 40);
    el.attr('fill', '#fff');
    el.attr('font-family','FontAwesome');
    
    if(tipo == "exclusive") {
      fbicon = "m 16,15 7.42857142857143,9.714285714285715 -7.42857142857143,9.714285714285715 3.428571428571429,0 5.714285714285715,-7.464228571428572 5.714285714285715,7.464228571428572 3.428571428571429,0 -7.42857142857143,-9.714285714285715 7.42857142857143,-9.714285714285715 -3.428571428571429,0 -5.714285714285715,7.464228571428572 -5.714285714285715,-7.464228571428572 -3.428571428571429,0 z";
      el2 = paper.path(fbicon).attr({fill: "black", stroke: "black"}).translate(x-5,y-5.5)
    }
    else if(tipo == "inclusive") {
      fbicon = "M15.898,4.045c-0.271-0.272-0.713-0.272-0.986,0l-4.71,4.711L5.493,4.045c-0.272-0.272-0.714-0.272-0.986,0s-0.272,0.714,0,0.986l4.709,4.711l-4.71,4.711c-0.272,0.271-0.272,0.713,0,0.986c0.136,0.136,0.314,0.203,0.492,0.203c0.179,0,0.357-0.067,0.493-0.203l4.711-4.711l4.71,4.711c0.137,0.136,0.314,0.203,0.494,0.203c0.178,0,0.355-0.067,0.492-0.203c0.273-0.273,0.273-0.715,0-0.986l-4.711-4.711l4.711-4.711C16.172,4.759,16.172,4.317,15.898,4.045z";
      el2 = paper.path(fbicon).attr({fill: "black", stroke: "black"}).translate(x-4.5,y-4)
    }
    else if(tipo == "parallel") {
      fbicon = "m 23,10 0,12.5 -12.5,0 0,3 12.5,0 0,12.5 3,0 0,-12.5 12.5,0 0,-3 -12.5,0 0,-12.5 -3,0 z"; 
      el2 = paper.path(fbicon).attr({fill: "black", stroke: "black"}).translate(x-4.5,y-4)
    }
    else {
      fbicon = "M18.344,16.174l-7.98-12.856c-0.172-0.288-0.586-0.288-0.758,0L1.627,16.217c0.339-0.543-0.603,0.668,0.384,0.682h15.991C18.893,16.891,18.167,15.961,18.344,16.174 M2.789,16.008l7.196-11.6l7.224,11.6H2.789z M10.455,7.552v3.561c0,0.244-0.199,0.445-0.443,0.445s-0.443-0.201-0.443-0.445V7.552c0-0.245,0.199-0.445,0.443-0.445S10.455,7.307,10.455,7.552M10.012,12.439c-0.733,0-1.33,0.6-1.33,1.336s0.597,1.336,1.33,1.336c0.734,0,1.33-0.6,1.33-1.336S10.746,12.439,10.012,12.439M10.012,14.221c-0.244,0-0.443-0.199-0.443-0.445c0-0.244,0.199-0.445,0.443-0.445s0.443,0.201,0.443,0.445C10.455,14.021,10.256,14.221,10.012,14.221";
      el2 = paper.path(fbicon).attr({fill: "black", stroke: "black"}).translate(x-4.5,y-4)
    }
      
    el2.node.setAttribute("class", "icongateway")
  }
   
}