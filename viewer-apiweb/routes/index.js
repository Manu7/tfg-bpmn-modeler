var express = require('express');
var router = express.Router();
var fs = require('fs');


/* GET modelador */

router.get('/nuevo', function(req, res, next) {

  var archivohtml = ""

  //Leer HTML
  fs.readFile('./public/index.html', 'utf8', function(err, data) {

    archivohtml = data;
    
    //Incluir SCRIPT al HTML aunque no haya diagrama y la librería que lo dibuja
    archivohtml = archivohtml + '<script language="javascript" src="js/diagramaviewer.js"></script></body></html>'

    res.send(archivohtml)
  });
  
  //res.sendfile('index.html', {root: 'public' })
});


/* POST modelador pasándole xml = cadena con el diagrama a pintar formato BPMN 2.0. */

router.post('/', function(req, res) {
  
  var archivohtml = ""

  //Leer HTML
  fs.readFile('./public/index.html', 'utf8', function(err, data) {
    if( err ){
      console.log(err)
      res.send("Error, archivo HTML no encontrado.").status(404)
    }
    else{
      archivohtml = data;
      
      //Incluir SCRIPT al HTML con los datos del diagrama y la librería que lo dibuja
      req.body.xml = req.body.xml.split("'").join('"');
      
      archivohtml = archivohtml + "<input type='text' id='diagrama' class='ocultar' value='" + req.body.xml + "'>"
      archivohtml = archivohtml + '<script language="javascript" src="js/diagramaviewer.js"></script></body></html>'

      res.send(archivohtml)
    }
  });
  
})

module.exports = router;
