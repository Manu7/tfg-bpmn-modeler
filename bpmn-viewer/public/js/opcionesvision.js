///////////////////////////////
// Pantalla completa del canvas
///////////////////////////////

$('#cambiarvistacanvas').click(function() {

	var elem = document.getElementById("canvas");
	var es_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
	var es_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
	var es_opera = navigator.userAgent.toLowerCase().indexOf('opera');
	var es_ie = navigator.userAgent.indexOf("MSIE") > -1 ;
	
	//Expandir canvas
	if($('#cambiarvistacanvas').hasClass('glyphicon-resize-full')) {

		$('#canvas').addClass('pantallacompleta')
		$('#cambiarvistacanvas').removeClass('glyphicon-resize-full')
		$('#cambiarvistacanvas').addClass('glyphicon-resize-small')
		
		if(es_chrome)
			elem.webkitRequestFullscreen();
		else if(es_firefox)
			elem.mozRequestFullScreen();
		else if(es_ie)
			elem.msRequestFullscreen();				
		else
			elem.requestFullscreen();
	
	} //Contraer canvas
	else {
		
		$('#canvas').removeClass('pantallacompleta')
		$('#cambiarvistacanvas').addClass('glyphicon-resize-full')
		$('#cambiarvistacanvas').removeClass('glyphicon-resize-small')

		if(es_chrome)
			document.webkitExitFullscreen();
		else if(es_firefox)
			document.mozCancelFullScreen();
		else if(es_ie)
			document.msExitFullscreen();
		else
			document.exitFullscreen();	
	}	
	
})

//Para salir de pantalla completa con ESC

$(document).keyup(function(e) {
  console.log("keyup");
     if (e.keyCode == 27) { // escape key maps to keycode `27`
       console.log("ESC");
       $('#canvas').removeClass('pantallacompleta')
       $('#cambiarvistacanvas').addClass('glyphicon-resize-full')
       $('#cambiarvistacanvas').removeClass('glyphicon-resize-small')
    }
});

/////////////////////////////////////
// Efecto persiana canvas-propiedades
/////////////////////////////////////

$(".canvas > div > a").attr("id","imagenlibreriaviewer");

$(function () {
    
    var p1 = parseInt($("#canvas").height());
    var p2 = parseInt($("#propertiesPanel").height());
    
    $(".divider").draggable({
        axis: "y",
        containment: "parent",
        scroll: false,
        drag: function () {
          console.log("position top");
          console.log(parseInt($(this).position().top));
          console.log("nav-persiana");
          console.log(parseInt($("#nav-persiana").height()));

          if (parseInt($(this).position().top) > parseInt($("#nav-persiana").height()))
          {
            var a = parseInt($(this).position().top-parseInt($("#nav-persiana").height()))+10;
            $("#canvas").css({height:a+10});
            $("#propertiesPanel").css({height:p1 + p2 -a});
            
            console.log("a:"+a)
            console.log("p1:"+p1)
            console.log("p1 height:"+parseInt($("#canvas").height()))
          }
          
          //Para ocultar la imagen del viewer cuando no cabe
          if (parseInt(parseInt($("#visordiagrama").height()))-parseInt($("#imagenlibreriaviewer").height()) <= parseInt($("#imagenlibreriaviewer").height())) {
            $("#imagenlibreriaviewer").addClass("ocultar");
          } else {
            if($("#imagenlibreriaviewer").hasClass("ocultar"))
              $("#imagenlibreriaviewer").removeClass("ocultar");
          }
          
            
        }
    });
})

///////////////////////
//Minimizar propiedades
///////////////////////

//Al clicar en boton
$('#minimizar').click(function() {

  $("#propertiesPanel").addClass('minimizar-properties')
  $("#propertiesPanel").removeClass('gexflow-properties-panel')
  $('#propertiesPanel').removeAttr('style');
  $("#propertiesBody").addClass('ocultar')

  $('#canvas').removeAttr('style');
  $("#canvas").removeClass('canvas-persiana')
  $("#canvas").removeClass('gexflow-canvas')
  $("#canvas").addClass('gexflow-big-canvas')

  $("#restaurar").removeClass('ocultar')
  $("#divider").removeAttr('style');
  $('#divider').removeClass('row_resize')
  $("#minimizar").addClass('ocultar')

})

$('#restaurar').click(function(){

  $("#propertiesPanel").removeClass('minimizar-properties')
  $("#propertiesPanel").addClass('gexflow-properties-panel')
  $("#propertiesBody").removeClass('ocultar')

  $("#canvas").removeClass('gexflow-big-canvas')
  $("#canvas").addClass('gexflow-canvas')
  $("#canvas").addClass('canvas-persiana')
  
  $("#restaurar").addClass('ocultar')
  $("#divider").removeAttr('style');
  $('#divider').addClass('row_resize')
  $("#minimizar").removeClass('ocultar')

})