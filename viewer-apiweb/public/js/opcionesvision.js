///////////////////////////////
// Pantalla completa del canvas
///////////////////////////////

$('#cambiarvistacanvas').click(function() {

	var elem = document.getElementById("canvas");
	var es_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
	var es_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
	var es_opera = navigator.userAgent.toLowerCase().indexOf('opera');
	var es_ie = navigator.userAgent.indexOf("MSIE") > -1 ;
	
	//Expandir canvas
	if($('#cambiarvistacanvas').hasClass('glyphicon-resize-full')) {

		$('#canvas').addClass('pantallacompleta')
		$('#cambiarvistacanvas').removeClass('glyphicon-resize-full')
		$('#cambiarvistacanvas').addClass('glyphicon-resize-small')
		
		if(es_chrome)
			elem.webkitRequestFullscreen();
		else if(es_firefox)
			elem.mozRequestFullScreen();
		else if(es_ie)
			elem.msRequestFullscreen();				
		else
			elem.requestFullscreen();
	
	} //Contraer canvas
	else {
		
		$('#canvas').removeClass('pantallacompleta')
		$('#cambiarvistacanvas').addClass('glyphicon-resize-full')
		$('#cambiarvistacanvas').removeClass('glyphicon-resize-small')

		if(es_chrome)
			document.webkitExitFullscreen();
		else if(es_firefox)
			document.mozCancelFullScreen();
		else if(es_ie)
			document.msExitFullscreen();
		else
			document.exitFullscreen();	
		
		$('.glyphicon-plus').click();
		$('.glyphicon-minus').click();
	}	
	
})

//Para salir de pantalla completa con ESC

$(document).keyup(function(e) {
  console.log("keyup");
     if (e.keyCode == 27) { // escape key maps to keycode `27`
       console.log("ESC");
       $('#canvas').removeClass('pantallacompleta')
       $('#cambiarvistacanvas').addClass('glyphicon-resize-full')
       $('#cambiarvistacanvas').removeClass('glyphicon-resize-small')
    
			 $('.glyphicon-plus').click();
			 $('.glyphicon-minus').click();
		 
		 }
});



//CSS//















/////////////////////////////////////
// Efecto persiana canvas-propiedades
/////////////////////////////////////

$(function () {
    
    var p1 = parseInt($("#canvas").height());
    var p2 = parseInt($("#propertiesPanel").height());
    
    $(".divider").draggable({
        axis: "y",
        containment: "parent",
        scroll: false,
        drag: function () {
          
          /*console.log("position top");
          console.log(parseInt($(this).position().top));
          console.log("nav-persiana");
          console.log(parseInt($("#nav-persiana").height()));
          
          console.log("canvas height:");
          console.log($("#canvas").height());
          console.log("Bigcanvas height:");
          console.log($(".gexflow-big-canvas").height());*/
          console.log("Mira el top: ", parseInt($(this).position().top), " mira el window height: ", parseInt($(window).height()))
          
          if((parseInt($(this).position().top) > parseInt($("#nav-persiana").height())+parseInt($("#nav-persiana").height())*0.4) && (parseInt($(this).position().top) <= parseInt($(window).height())-parseInt($("#nav-persiana").height())*1.35))
          {
            var a = parseInt($(this).position().top-parseInt($("#nav-persiana").height()))+10;
            $("#canvas").css({height:a+10});
            $(".canvas").css({height:a+10});
            console.log("canvas height:");
            console.log(parseInt($("#canvas").height()));
            console.log("Canvas top");
            console.log(parseInt($("#canvas").top));
            var bjsTop = parseInt($(".navbar").height()) + 2*parseInt($(".panel-heading").height() + 5);
            $(".bjs-container").css({height: parseInt($("#canvas").height()-parseInt($(".panel-heading").height())-16)});
            $(".djs-container").css({height: parseInt($("#canvas").height()-parseInt($(".panel-heading").height())-16)});
            $(".bjs-container").css({top: bjsTop});
            $(".djs-container").css({top: bjsTop});
            $(".bjs-container").css({position:"fixed"});
            $(".djs-container").css({position:"fixed"});
            $("#propertiesPanel").css({height:p1 + p2 -a});
            
            console.log("a:"+a)
            console.log("p1:"+p1)
            console.log("p1 height:"+parseInt($("#canvas").height()))
          }
          
          
          
          //Para ocultar la imagen del viewer cuando no cabe
          if (parseInt(parseInt($("#visordiagrama").height()))-parseInt($(".bjs-container > a").height())-100 <= parseInt($(".bjs-container > a").height())) {
            $(".bjs-container > a").addClass("ocultar");
          } else {
            if($(".bjs-container > a").hasClass("ocultar"))
              $(".bjs-container > a").removeClass("ocultar");
          }
          
            
        }
    });
})

///////////////////////
//Minimizar propiedades
///////////////////////

//Al clicar en boton
$('#minimizar').click(function() {

  $("#propertiesPanel").addClass('minimizar-properties')
  $("#propertiesPanel").removeClass('gexflow-properties-panel')
  $('#propertiesPanel').removeAttr('style');
  $("#propertiesBody").addClass('ocultar')

  $('#canvas').removeAttr('style');
  $("#canvas").removeClass('canvas-persiana')
  $("#canvas").removeClass('gexflow-canvas')
  $("#canvas").addClass('gexflow-big-canvas')

  $("#restaurar").removeClass('ocultar')
  $("#divider").removeAttr('style');
  $('#divider').removeClass('row_resize')
  $("#minimizar").addClass('ocultar')

})

$('#restaurar').click(function(){

  $("#propertiesPanel").removeClass('minimizar-properties')
  $("#propertiesPanel").addClass('gexflow-properties-panel')
  $("#propertiesBody").removeClass('ocultar')

  $("#canvas").removeClass('gexflow-big-canvas')
  $("#canvas").addClass('gexflow-canvas')
  $("#canvas").addClass('canvas-persiana')
  
  $("#restaurar").addClass('ocultar')
  $("#divider").removeAttr('style');
  $('#divider').addClass('row_resize')
  $("#minimizar").removeClass('ocultar')

})



//////////////////
//MINIMIZAR PALETA
//////////////////
$(document).ready ( function(){
  
  $('.djs-palette-entries').append("<div id='minpaleta' class='paletaminimizar'>");
  $('.djs-palette-entries').append("<span id='minpaletaicon' class='glyphicon glyphicon-menu-left'></span> </div>");
  
  function minmaxPaleta() {
    console.log("HAHHAHA")
    if($('#minpaletaicon').hasClass('glyphicon-menu-left')) {
      console.log("1")
      //Ocultamos y cambiamos icono
      $('.djs-palette').css("visibility", "hidden");
      $('#minpaletaicon').removeClass('glyphicon-menu-left');
      $('#minpaletaicon').addClass('glyphicon-menu-right');
      $('.djs-palette').css("margin-left", "-20");          
      $('#minpaletaicon').css("margin-left", "-2.5%");
      $('#minpaleta').css("margin-left", "-26.4%");

    } else {
            console.log("2")

      //Hacemos visible y cambiamos icono
      $('.djs-palette').css("visibility", "visible");
      $('#minpaletaicon').addClass('glyphicon-menu-left');
      $('#minpaletaicon').removeClass('glyphicon-menu-right');
      $('.djs-palette').css("margin-left", "-19");
      $('#minpaletaicon').css("margin-left", "99.5%");
      $('#minpaleta').css("margin-left", "73%");
      
    }
  }
  
  $('#minpaleta').on("click", minmaxPaleta)
  $('#minpaletaicon').on("click", minmaxPaleta)
  
  
  
})



/////////////////
//VER PROPIEDADES
/////////////////
$(document).ready ( function(){
		
		/*$(document).click(function(){
    
				if($('.intro').css("display") == "none") {
						$('.form-bas-div').css('display', 'table');
				} else {
						$('.form-bas-div').css('display', 'none');
				}
		})*/
		
		function checkForChangesDiagram()
		{
				if($('.canvas').css("visibility") == "hidden") {
						$('.form-bas-div').css('display', 'none');
						setTimeout(checkForChangesDiagram, 500);
				} else {
						$('.form-bas-div').css('display', 'table');
						//setTimeout(checkForChangesDiagram, 500);
				}
				
				console.log("HOLA")
				console.log($('.intro').css("display"))
				console.log($('.intro').css("display") == "none")
		}
		
		checkForChangesDiagram();
		
})


//////////////////
//ABRIR SUBPROCESO
//////////////////
$(document).ready ( function(){
  
  var bjsTop = parseInt($(".navbar").height()) + 2*parseInt($(".panel-heading").height() + 5);
$(".bjs-container").css({height: parseInt($("#canvas").height()-parseInt($(".panel-heading").height())-16)});
$(".djs-container").css({height: parseInt($("#canvas").height()-parseInt($(".panel-heading").height())-16)});
$(".bjs-container").css({top: bjsTop});
$(".djs-container").css({top: bjsTop});
$(".bjs-container").css({position:"fixed"});
$(".djs-container").css({position:"fixed"});

  $('g').click(function() {

    console.log("hola ha habido click")
    console.log(this)

  })
})
